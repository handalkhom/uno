# No. 1
Mampu mendemonstrasikan penyelesaian masalah dengan pendekatan matematika dan algoritma pemrograman secara tepat
![](https://gitlab.com/handalkhom/uno/-/raw/master/gif%20uts/1.gif)

# No. 2
Mampu menjelaskan algoritma dari solusi yang dibuat
![](https://gitlab.com/handalkhom/uno/-/raw/master/gif%20uts/2.gif)

# No. 3
Mampu menjelaskan konsep dasar OOP
OOP (Object-Oriented Programming) adalah paradigma pemrograman yang berfokus pada pengorganisasian kode dalam bentuk objek yang memiliki atribut dan perilaku tertentu. Konsep dasar OOP meliputi:

1. Objek: Objek adalah instansi dari sebuah kelas. Sebuah objek memiliki atribut (data) yang menggambarkan keadaan objek tersebut dan metode (fungsi) yang menggambarkan perilaku atau tindakan yang dapat dilakukan oleh objek tersebut.

2. Kelas: Kelas adalah blueprint atau cetak biru yang mendefinisikan struktur dan perilaku umum dari sebuah objek. Kelas mendefinisikan atribut dan metode yang akan dimiliki oleh objek-objek yang dibuat berdasarkan kelas tersebut.

3. Abstraksi: Abstraksi digunakan untuk mendeklarasikan sebuah kelas yang menyimpan method abstrak yang isinya masih kosong dan belum bisa digunakan. Untuk mengimplementasikan method abstrak dapat digunakan konsep inheritance  

4. Enkapsulasi: Enkapsulasi adalah konsep yang menggabungkan data dan fungsi-fungsi terkait dalam satu unit yang disebut objek. Enkapsulasi melindungi data dari akses langsung oleh kode lain, sehingga hanya metode-metode yang ditentukan dalam kelas yang dapat mengubah atau mengakses data tersebut.

5. Pewarisan: Pewarisan memungkinkan pembuatan kelas baru yang mewarisi atribut dan metode dari kelas yang sudah ada. Dengan menggunakan pewarisan, kita dapat membuat hierarki kelas, di mana kelas yang baru dibuat dapat memiliki atribut dan metode dari kelas dasar (induk) serta menambahkan atau mengubah perilaku yang spesifik.

6. Polimorfisme: Polimorfisme memungkinkan objek untuk memiliki banyak bentuk. Dalam konteks OOP, ini berarti objek dari kelas yang berbeda dapat diproses menggunakan metode yang sama, menghasilkan perilaku yang sesuai dengan jenis objek yang digunakan.

Dengan menggunakan konsep-konsep dasar OOP ini, kita dapat membangun program yang lebih terstruktur, modular, dan mudah dimengerti serta mengelola kompleksitas dalam pengembangan perangkat lunak.

# No. 4
Mampu mendemonstrasikan penggunaan Encapsulation secara tepat
![](https://gitlab.com/handalkhom/uno/-/raw/master/gif%20uts/4.gif)

# No. 5
Mampu mendemonstrasikan penggunaan Abstraction secara tepat
![](https://gitlab.com/handalkhom/uno/-/raw/master/gif%20uts/5.gif)

# No. 6
Mampu mendemonstrasikan penggunaan Inheritance dan Polymorphism secara tepat
![](https://gitlab.com/handalkhom/uno/-/raw/master/gif%20uts/6.gif)

# No. 7
Mampu menerjemahkan proses bisnis ke dalam skema OOP secara 
tepat. Bagaimana cara Kamu mendeskripsikan proses bisnis 
(kumpulan use case) ke dalam OOP 

Kumpulan Use Case
Use Case | Nilai Prioritas
--- | ---
Terdapat card deck yang dishuffle di awal permainan  | 10
Kartu dibagikan kepada player di awal permainan  | 10
Player dapat mengeluarkan kartu yang cocok dengan kartu pada table | 9
Dapat memilih jumlah player | 10
Player dapat menentukan nama masing-masing | 7
Player bergiliran menaruh kartu yang tepat pada table | 10
Terdapat kartu power-up (Skip, Reverse, Wild, +2, +4) | 10
Arah giliran berubah oleh kartu reverse | 10
Ketika kartu Skip ditaruh, pemain giliran selanjutnya akan dilewat 1 putaran | 10
Kartu +2 dan +4 membuat pemain harus mengambil kartu sesuai akumulasi | 10
Player dapat mengganti warna kartu dengan kartu Wild | 9
Player akan dianggap menang jika kartu di tangan sudah habis | 10
Permainan berakhir ketika ada pemenang | 10

# No. 8
Mampu menjelaskan rancangan dalam bentuk Class Diagram, dan Use Case table

```mermaid
flowchart TD
    1["Jalankan aplikasi"] --> 2["Klik mulai"] 
    2 --> 3["Masukkan nama pemain"]
    3 --> 4["Player bergiliran mengeluarkan kartu sesuai aturan"]
    4 --> 5["Player pertama dengan kartu habis menang"]
    5 --> 6["Game selesai"]
```

```mermaid
classDiagram
    class Uno {
        +main()
    }

    class UnoCardObject {
        -boolean isDrawFour
        -boolean isWild
        +UnoCardObject()
        +isDrawFour()
        +isWild()
        +checkClass()
    }

    class UnoCard { 
        +enum Color
        +enum Value
        -final Color color
        -final Value value
        +UnoCard(final Color color, final Value value)
        +getColor()
        +getValue()
        +toString()
        -checkClass()
    }

    class UnoDeck { 
        -UnoCard[] cards
        -int cardsInDeck
        +UnoDeck()
        +reset()
        +replaceDeckWith(List~UnoCard~ cards)
        +isEmpty()
        +shuffle()
        +drawCard()
        +drawCardImage()
        +drawCard(int n)
        +checkClass()
    }

    Game { 
        -int currentPlayer
        -String[] playerIds
        -UnoDeck deck
        -List~List~UnoCard~~ playerHand
        -List~UnoCard~ stockpile
        -UnoCard.Color validColor
        -UnoCard.Value validValue
        +boolean gameDirection
        +Game(String[] pids)
        +start(Game game)
        +getTopCard()
        +getTopCardImage()
        +isGameOver()
        +getCurrentPlayer()
        +getPreviousPlayer()
        +getPlayers()
        +getPlayerHand(String pid)
        +getPlayerHandSize(String pid)
        +getPlayerCard(String pid, int choice)
        +hasEmptyHand(String pid)
        +validCardPlay(UnoCard card)
        +checkPlayerTurn(String pid)
        +submitDraw(String pid)
        +setCardColor(UnoCard.Color color)
        +submitPlayerCard(String pid, UnoCard card, UnoCard.Color declaredColor)
        +checkClass()
    }

    class Menu {
        -javax.swing.JButton jButton1
        -javax.swing.JButton jButton2
        -javax.swing.JLabel jLabel1
        -javax.swing.JPanel jPanel1
        +Menu()
        -initComponents()
        -jButton1ActionPerformed()
        -jButton2ActionPerformed()
        +main()
    }

    class AddPlayerName {
        -javax.swing.JButton doneButton
        -javax.swing.JLabel jLabel1
        -javax.swing.JLabel jLabel2
        -javax.swing.JPanel jPanel1
        -javax.swing.JLabel pIdFourLabel
        -javax.swing.JLabel pIdOneLabel
        -javax.swing.JTextField pIdTextBox
        -javax.swing.JLabel pIdThreeLabel
        -javax.swing.JLabel pIdTwoLabel
        -javax.swing.JButton saveButton
        +List~String~ playerIds
        +AddPlayerName()
        +getPids()
        -initComponents()
        -pIdTextBoxActionPerformed()
        -saveButtonActionPerformed()
        -doneButtonActionPerformed()
        -main()
    }

    class GameStage {
        -javax.swing.JButton downCard
        -javax.swing.JButton drawCardButton
        -javax.swing.JButton jButton1
        -javax.swing.JButton jButton10
        -javax.swing.JButton jButton11
        -javax.swing.JButton jButton12
        -javax.swing.JButton jButton13
        -javax.swing.JButton jButton14
        -javax.swing.JButton jButton15
        -javax.swing.JButton jButton2
        -javax.swing.JButton jButton3
        -javax.swing.JButton jButton4
        -javax.swing.JButton jButton5
        -javax.swing.JButton jButton6
        -javax.swing.JButton jButton7
        -javax.swing.JButton jButton8
        -javax.swing.JButton jButton9
        -javax.swing.JPanel jPanel1
        -javax.swing.JLabel pidNameLabel
        -javax.swing.JButton topCardButton
        -AddPlayerName addPlayer = new AddPlayerName()
        +List~String~ temp = new List~ ~()
        +String[] pids
        +Game game
        +List~JButton~ cardButtons = new List~JButton~()
        +List~String~ cardIds
        +PopUp window
        +GameStage()
        +GameStage(List~String~playerIds)
        +setButtonIcons()
        +populateArrayList()
        +setPidName()
        +setPidName(currentPlayer)
        -initComponents()
        -drawCardButtonActionPerformed()
        -jButton1ActionPerformed()
        -jButton2ActionPerformed()
        -jButton3ActionPerformed()
        -jButton4ActionPerformed()
        -jButton5ActionPerformed()
        -jButton6ActionPerformed()
        -jButton7ActionPerformed()
        -jButton8ActionPerformed()
        -jButton9ActionPerformed()
        -jButton10ActionPerformed()
        -jButton11ActionPerformed()
        -jButton12ActionPerformed()
        -jButton13ActionPerformed()
        -jButton14ActionPerformed()
        -jButton15ActionPerformed()
        -downCardActionPerformed()
        -topCardActionPerformed()
        +main()
    }

    class PopUp {
        -javax.swing.JButton cancelButton
        -javax.swing.JLabel cardLabel
        -javax.swing.JPanel jPanel1
        -javax.swing.JButton useCardButton
        +PopUp()
        +PopUp(cardName, game, index, cardButtons, gameStage, topCardButton)
        -useCardButtonActionPerformed()
        -cancelButtonActionPerformed()
        +main()
    }

    class PickColorFrame {
        -javax.swing.JButton blueButton
        -javax.swing.JButton greenButton
        -javax.swing.JLabel jLabel1
        -javax.swing.JButton redButton
        -javax.swing.JButton yellowButton
        -UnoCard.Color wildColor = null
        +boolean allow = false
        +PopUp popUp
        +PickColorFrame()
        +PickColorFrame(pop)
        +UnoCard.Color choseColor(UnoCard card)
        +initComponents()
        -redButtonActionPerformed()
        -blueButtonActionPerformed()
        -greenButtonActionPerformed()
        -yellowButtonActionPerformed()
        +main()
    }

    UnoCardObject <|-- UnoCard : Inheritance
    UnoCardObject <|-- UnoDeck : Inheritance
    UnoCardObject <|-- Game : Inheritance
    Uno --> UnoCard : Menjalankan
    Uno --> UnoDeck : Menjalankan
    Uno --> Game : Menjalankan
    Uno --> Menu : Menjalankan
    Uno --> GameStage : Menjalankan
    Uno --> AddPlayerName : Menjalankan
    Uno --> PickColorFrame : Menjalankan
    Uno --> PopUp : Menjalankan
```


# No. 9
Mampu memberikan gambaran umum aplikasi kepada publik menggunakan presentasi berbasis video.

Link : https://youtu.be/aq6bp4qcFJQ

# No. 10
Inovasi UX 

![](https://gitlab.com/handalkhom/uno/-/raw/master/gif%20uts/Screenshot_2023-05-13_215626.png)
![](https://gitlab.com/handalkhom/uno/-/raw/master/gif%20uts/Screenshot_2023-05-13_215642.png)
![](https://gitlab.com/handalkhom/uno/-/raw/master/gif%20uts/Screenshot_2023-05-13_215653.png)

# Source Code
Link Repo: [Source Code Keseluruhan Project](https://gitlab.com/handalkhom/uno.git)
