/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.uno;

public abstract class UnoCardObject {

    private boolean isWild;
    private boolean isDrawFour;

    public UnoCardObject() {
        isWild = false;
        isDrawFour = false;
    }

    public boolean isWild() {
        return isWild;
    }

    public boolean isDrawFour() {
        return isDrawFour;
    }

    public abstract void checkClass();
}


